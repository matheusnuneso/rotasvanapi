package com.RotasVanApi.service;

import com.RotasVanApi.dto.address.AddressDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.AddressModel;
import com.RotasVanApi.repository.AddressRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AddressServiceTest {

    @Mock
    private AddressRepository addressRepository;

    @InjectMocks
    private AddressService addressService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateSuccess() {
        AddressDto addressDto = new AddressDto();
        addressDto.setLongitude(String.valueOf(10.0));
        addressDto.setLatitude(String.valueOf(20.0));

        AddressModel addressModel = new AddressModel(addressDto);
        GeometryFactory geometryFactory = new GeometryFactory();
        Point point = geometryFactory.createPoint(new Coordinate(Double.parseDouble(addressDto.getLongitude()), Double.parseDouble(addressDto.getLatitude())));
        point.setSRID(4326);
        addressModel.setLocationPostgis(point);

        when(addressRepository.save(any(AddressModel.class))).thenReturn(addressModel);

        AddressModel result = addressService.create(addressDto);

        assertNotNull(result);
        assertEquals(point, result.getLocationPostgis());
        verify(addressRepository, times(1)).save(any(AddressModel.class));
    }

    @Test
    void testFindByIdSuccess() {
        UUID addressId = UUID.randomUUID();
        AddressModel addressModel = new AddressModel();
        when(addressRepository.findById(addressId)).thenReturn(Optional.of(addressModel));

        AddressModel result = addressService.findById(addressId);

        assertNotNull(result);
        verify(addressRepository, times(1)).findById(addressId);
    }

    @Test
    void testFindByIdNotFound() {
        UUID addressId = UUID.randomUUID();
        when(addressRepository.findById(addressId)).thenReturn(Optional.empty());

        assertThrows(CustomNotFoundException.class, () -> addressService.findById(addressId));
        verify(addressRepository, times(1)).findById(addressId);
    }
}
package com.RotasVanApi.service;

import com.RotasVanApi.dto.address.AddressDto;
import com.RotasVanApi.dto.client.ClientNotifDto;
import com.RotasVanApi.dto.van.VanSaveDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.infra.notifications.NotificationService;
import com.RotasVanApi.model.NotificationsSent;
import com.RotasVanApi.model.VanModel;
import com.RotasVanApi.repository.VanRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class VanServiceTest {

    @Mock
    private VanRepository vanRepository;

    @Mock
    private NotificationService notificationService;

    @Mock
    private NotificationSentService notificationSentService;

    @InjectMocks
    private VanService vanService;

    private VanSaveDto vanSaveDto;
    private VanModel vanModel;
    private UUID vanId;

    @BeforeEach
    public void setup() {
        vanSaveDto = new VanSaveDto();
        vanModel = new VanModel(vanSaveDto);
        vanId = UUID.randomUUID();
    }

    @Test
    public void testCreate() {
        when(vanRepository.save(any(VanModel.class))).thenReturn(vanModel);

        VanModel createdVan = vanService.create(vanSaveDto);

        assertNotNull(createdVan);
        verify(vanRepository, times(1)).save(any(VanModel.class));
    }

    @Test
    public void testFindById_VanExists() {
        when(vanRepository.findById(vanId)).thenReturn(Optional.of(vanModel));

        VanModel foundVan = vanService.findById(vanId);

        assertNotNull(foundVan);
        verify(vanRepository, times(1)).findById(vanId);
    }

    @Test
    public void testFindById_VanNotExists() {
        when(vanRepository.findById(vanId)).thenReturn(Optional.empty());

        CustomNotFoundException thrown = assertThrows(CustomNotFoundException.class, () -> {
            vanService.findById(vanId);
        });

        assertEquals("Van not found!", thrown.getMessage());
    }

    @Test
    public void testChangeStatusTrip() {
        when(vanRepository.findById(vanId)).thenReturn(Optional.of(vanModel));

        vanService.changeStatusTrip(vanId, true);

        assertTrue(vanModel.getStartedTrip());
        verify(vanRepository, times(1)).save(vanModel);
    }

    @Test
    public void testUpdateCurrentLocation_ValidCoordinates() {
        AddressDto location = new AddressDto();
        location.setLatitude("40.7128");
        location.setLongitude("-74.0060");

        when(vanRepository.save(any(VanModel.class))).thenReturn(vanModel);

        vanService.updateCurrentLocation(vanModel, location);

        assertEquals("40.7128", vanModel.getCurrentLatitude());
        assertEquals("-74.0060", vanModel.getCurrentLongitude());
        verify(vanRepository, times(1)).save(vanModel);
    }

    @Test
    public void testUpdateCurrentLocation_InvalidCoordinates() {
        AddressDto location = new AddressDto();
        location.setLatitude("invalid");
        location.setLongitude("invalid");

        DateTimeException thrown = assertThrows(DateTimeException.class, () -> {
            vanService.updateCurrentLocation(vanModel, location);
        });

        assertEquals("Incorret coordinate format!", thrown.getMessage());
    }

    @Test
    public void testIsValidLatitude() {
        assertTrue(vanService.isValidLatitude("40.7128"));
        assertFalse(vanService.isValidLatitude("invalid"));
    }

    @Test
    public void testIsValidLongitude() {
        assertTrue(vanService.isValidLongitude("-74.0060"));
        assertFalse(vanService.isValidLongitude("invalid"));
    }

    /*@Test
    public void testReturnTokensClientsNearVan() {
        LocalDate todayDate = LocalDate.now();
        ArrayList<ClientNotifDto> clientsTokens = new ArrayList<>();
        when(vanRepository.returnClientsNearVan(anyDouble(), anyDouble(), anyInt(), any(UUID.class), any(LocalDate.class)))
                .thenReturn(clientsTokens);

        List<ClientNotifDto> result = vanService.returnTokensClientsNearVan(vanModel, todayDate, 5000);

        assertNotNull(result);
        verify(vanRepository, times(1)).returnClientsNearVan(anyDouble(), anyDouble(), anyInt(), any(UUID.class), any(LocalDate.class));
    }*/
}
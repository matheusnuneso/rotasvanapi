package com.RotasVanApi.service;

import com.RotasVanApi.dto.user.UserAuthDto;
import com.RotasVanApi.dto.user.UserSaveDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.UserModel;
import com.RotasVanApi.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreate() {
        UserSaveDto userSaveDto = new UserSaveDto();
        UserModel userModel = new UserModel();
        when(userRepository.save(any(UserModel.class))).thenReturn(userModel);

        UserModel result = userService.create(userSaveDto);

        assertNotNull(result);
        verify(userRepository, times(1)).save(any(UserModel.class));
    }

    @Test
    void testDelete() {
        UUID id = UUID.randomUUID();
        doNothing().when(userRepository).deleteById(id);

        userService.delete(id);

        verify(userRepository, times(1)).deleteById(id);
    }

    @Test
    void testExistsByUserName() {
        String userName = "testUser";
        when(userRepository.existsByUserName(userName)).thenReturn(true);

        boolean result = userService.existsByUserName(userName);

        assertTrue(result);
        verify(userRepository, times(1)).existsByUserName(userName);
    }

    @Test
    void testFindByUserName() {
        String userName = "testUser";
        UserModel userModel = new UserModel();
        when(userRepository.findByUserName(userName)).thenReturn(userModel);

        UserDetails result = userService.findByUserName(userName);

        assertNotNull(result);
        verify(userRepository, times(1)).findByUserName(userName);
    }

    @Test
    void testAuthenticationUser() {
        UserAuthDto userAuthDto = new UserAuthDto();
        userAuthDto.setPassword("password");
        UserModel userModel = new UserModel();
        userModel.setPassword("password");

        assertDoesNotThrow(() -> userService.authenticationUser(userAuthDto, userModel));
    }

    @Test
    void testAuthenticationUserIncorrectPassword() {
        UserAuthDto userAuthDto = new UserAuthDto();
        userAuthDto.setPassword("wrongPassword");
        UserModel userModel = new UserModel();
        userModel.setPassword("password");

        assertThrows(CustomNotFoundException.class, () -> userService.authenticationUser(userAuthDto, userModel));
    }
}
package com.RotasVanApi.service;

import com.RotasVanApi.dto.driver.DriverResponseDto;
import com.RotasVanApi.dto.driver.DriverSaveDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.DriverModel;
import com.RotasVanApi.model.UserModel;
import com.RotasVanApi.model.VanModel;
import com.RotasVanApi.repository.DriverRepository;
import jakarta.persistence.EntityExistsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DriverServiceTest {

    @Mock
    private DriverRepository driverRepository;

    @Mock
    private UserService userService;

    @Mock
    private VanService vanService;

    @InjectMocks
    private DriverService driverService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateUserNameExists() {
        DriverSaveDto driverSaveDto = new DriverSaveDto();
        driverSaveDto.setUserName("existingUser");
        when(userService.existsByUserName(driverSaveDto.getUserName())).thenReturn(true);

        assertThrows(EntityExistsException.class, () -> driverService.create(driverSaveDto));
        verify(userService, times(1)).existsByUserName(driverSaveDto.getUserName());
    }

    @Test
    void testCreateSuccess() {
        DriverSaveDto driverSaveDto = new DriverSaveDto();
        driverSaveDto.setUserName("newUser");
        when(userService.existsByUserName(driverSaveDto.getUserName())).thenReturn(false);

        UserModel userModel = new UserModel();
        userModel.setUserName(driverSaveDto.getUserName());
        when(userService.create(any())).thenReturn(userModel);

        DriverModel driverModel = new DriverModel();
        driverModel.setId(UUID.randomUUID());
        driverModel.setUser(userModel);
        driverModel.setVanModel(new VanModel());
        driverModel.setName(driverSaveDto.getName());
        when(driverRepository.save(any(DriverModel.class))).thenReturn(driverModel);

        DriverResponseDto result = driverService.create(driverSaveDto);

        assertNotNull(result);
        assertEquals(driverSaveDto.getUserName(), result.getUserName());
        verify(userService, times(1)).existsByUserName(driverSaveDto.getUserName());
        verify(driverRepository, times(1)).save(any(DriverModel.class));
    }
}
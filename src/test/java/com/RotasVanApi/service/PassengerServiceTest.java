package com.RotasVanApi.service;

import com.RotasVanApi.dto.client.ClientResponseDto;
import com.RotasVanApi.dto.passenger.PassengerResponseDto;
import com.RotasVanApi.dto.passenger.PassengerSaveDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.*;
import com.RotasVanApi.repository.PassengerRepository;
import org.checkerframework.checker.units.qual.A;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PassengerServiceTest {

    @Mock
    private PassengerRepository passengerRepository;

    @Mock
    private ClientService clientService;

    @Mock
    private VanService vanService;

    @Mock
    private AddressService addressService;

    @Mock
    private DriverService driverService;

    @InjectMocks
    private PassengerService passengerService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateSuccess() {
        PassengerSaveDto passengerSaveDto = new PassengerSaveDto();
        UserModel userModel = new UserModel();
        userModel.setUserName("randomUserName");
        ClientModel clientModel = new ClientModel();
        clientModel.setUser(userModel);
        VanModel vanModel = new VanModel();
        AddressModel addressModel = new AddressModel();
        ClientResponseDto clientResponseDto = new ClientResponseDto(clientModel);

        when(clientService.findById(any(UUID.class))).thenReturn(clientResponseDto);
        when(vanService.findById(any(UUID.class))).thenReturn(vanModel);
        when(addressService.findById(any(UUID.class))).thenReturn(addressModel);

        PassengerModel passengerModel = new PassengerModel();
        passengerModel.setClientModel(clientModel);
        passengerModel.setVanModel(vanModel);
        passengerModel.setOriginAddressModel(addressModel);
        passengerModel.setDestinyAddressModel(addressModel);
        when(passengerRepository.save(any(PassengerModel.class))).thenReturn(passengerModel);

        PassengerResponseDto result = passengerService.create(passengerSaveDto);

        assertNotNull(result);
    }

    @Test
    void testFindByIdSuccess() {
        UUID passengerId = UUID.randomUUID();

        ClientModel clientModel = new ClientModel();
        clientModel.setId(UUID.randomUUID());

        VanModel vanModel = new VanModel();
        vanModel.setId(UUID.randomUUID());

        AddressModel addressModel = new AddressModel();
        addressModel.setId(UUID.randomUUID());

        PassengerModel passengerModel = new PassengerModel();
        passengerModel.setClientModel(clientModel);
        passengerModel.setVanModel(vanModel);
        passengerModel.setOriginAddressModel(addressModel);
        passengerModel.setDestinyAddressModel(addressModel);
        when(passengerRepository.findById(passengerId)).thenReturn(Optional.of(passengerModel));

        PassengerResponseDto result = passengerService.findById(passengerId);

        assertNotNull(result);
        verify(passengerRepository, times(1)).findById(passengerId);
    }

    @Test
    void testFindByIdNotFound() {
        UUID passengerId = UUID.randomUUID();
        when(passengerRepository.findById(passengerId)).thenReturn(Optional.empty());

        assertThrows(CustomNotFoundException.class, () -> passengerService.findById(passengerId));
        verify(passengerRepository, times(1)).findById(passengerId);
    }
}
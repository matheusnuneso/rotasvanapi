package com.RotasVanApi.service;

import com.RotasVanApi.dto.client.ClientResponseDto;
import com.RotasVanApi.dto.client.ClientSaveDto;
import com.RotasVanApi.dto.user.UserSaveDto;
import com.RotasVanApi.enums.RoleEnum;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.ClientModel;
import com.RotasVanApi.model.UserModel;
import com.RotasVanApi.repository.ClientRepository;
import jakarta.persistence.EntityExistsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ClientServiceTest {

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private UserService userService;

    @InjectMocks
    private ClientService clientService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateUserNameExists() {
        ClientSaveDto clientSaveDto = new ClientSaveDto();
        clientSaveDto.setUserName("existingUser");
        when(userService.existsByUserName(clientSaveDto.getUserName())).thenReturn(true);

        assertThrows(EntityExistsException.class, () -> clientService.create(clientSaveDto));
        verify(userService, times(1)).existsByUserName(clientSaveDto.getUserName());
    }

    @Test
    void testCreateSuccess() {
        ClientSaveDto clientSaveDto = new ClientSaveDto();
        clientSaveDto.setUserName("newUser");
        when(userService.existsByUserName(clientSaveDto.getUserName())).thenReturn(false);

        UserModel userModel = new UserModel();
        userModel.setUserName(clientSaveDto.getUserName());
        when(userService.create(any(UserSaveDto.class))).thenReturn(userModel);

        ClientModel clientModel = new ClientModel();
        clientModel.setUser(userModel);
        when(clientRepository.save(any(ClientModel.class))).thenReturn(clientModel);

        ClientResponseDto result = clientService.create(clientSaveDto);

        assertNotNull(result);
        assertEquals(clientSaveDto.getUserName(), result.getUserName());
        verify(userService, times(1)).existsByUserName(clientSaveDto.getUserName());
        verify(clientRepository, times(1)).save(any(ClientModel.class));
    }

    @Test
    void testFindByIdSuccess() {
        UserModel userModel = new UserModel();
        userModel.setUserName("randomuserName");

        UUID clientId = UUID.randomUUID();
        ClientModel clientModel = new ClientModel();
        clientModel.setUser(userModel);

        when(clientRepository.findById(clientId)).thenReturn(Optional.of(clientModel));

        ClientResponseDto result = clientService.findById(clientId);

        assertNotNull(result);
        verify(clientRepository, times(1)).findById(clientId);
    }

    @Test
    void testFindByIdNotFound() {
        UUID clientId = UUID.randomUUID();
        when(clientRepository.findById(clientId)).thenReturn(Optional.empty());

        assertThrows(CustomNotFoundException.class, () -> clientService.findById(clientId));
        verify(clientRepository, times(1)).findById(clientId);
    }
}
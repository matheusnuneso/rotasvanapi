package com.RotasVanApi.controller;

import com.RotasVanApi.dto.passenger.PassengerResponseDto;
import com.RotasVanApi.dto.user.AuthResponseDto;
import com.RotasVanApi.dto.user.UserAuthDto;
import com.RotasVanApi.dto.user.UserSaveDto;
import com.RotasVanApi.enums.RoleEnum;
import com.RotasVanApi.infra.security.TokenService;
import com.RotasVanApi.model.UserModel;
import com.RotasVanApi.service.ClientService;
import com.RotasVanApi.service.DriverService;
import com.RotasVanApi.service.PassengerService;
import com.RotasVanApi.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "api/auth")
public class AuthController {

    @Autowired
    private TokenService tokenService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private DriverService driverService;
    @Autowired
    private PassengerService passengerService;

    @PostMapping("/register")
    public ResponseEntity<Object> create(@RequestBody @Valid UserSaveDto userSaveDto){
        if (userService.existsByUserName(userSaveDto.getUserName())){
            return ResponseEntity.badRequest().build();
        }

        String encryptedPassword = new BCryptPasswordEncoder().encode(userSaveDto.getPassword());
        userSaveDto.setPassword(encryptedPassword);

        UserModel newUser = userService.create(userSaveDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(newUser);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthResponseDto> login(@RequestBody @Valid UserAuthDto userAuthDto){
        var userNamePassword = new UsernamePasswordAuthenticationToken(userAuthDto.getUserName(), userAuthDto.getPassword());
        var auth = authenticationManager.authenticate(userNamePassword);

        UserModel user = (UserModel) auth.getPrincipal();

        var token = tokenService.generateToken(user);
        UUID userId = getUserId(user);
        List<UUID> passengers = getPassengerList(user, userId);
        String tokenFirebase = getTokenFirebase(userId, user);

        AuthResponseDto tokenResponse = new AuthResponseDto(userId, user.getRole(), token, passengers, tokenFirebase);
        return ResponseEntity.status(HttpStatus.OK).body(tokenResponse);
    }

    private UUID getUserId(UserModel user){
        UUID userId = null;
        if (user.getRole().equals(RoleEnum.CLIENT)){
            userId = clientService.findByUserId(user.getId()).getId();

        } else if (user.getRole().equals(RoleEnum.DRIVER)) {
            userId = driverService.findByUserId(user.getId()).getId();
        }
        return userId;
    }

    private List<UUID> getPassengerList(UserModel user, UUID userId) {
        if (user.getRole().equals(RoleEnum.CLIENT)) {

            return passengerService.findByClientId(userId).stream()
                    .map(PassengerResponseDto::getId)
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    private String getTokenFirebase(UUID userId, UserModel user) {
        if (user.getRole().equals(RoleEnum.CLIENT)){
            return clientService.getTokenFirebase(userId);
        }
        return "";
    }
}

package com.RotasVanApi.controller;

import com.RotasVanApi.dto.client.ClientResponseDto;
import com.RotasVanApi.dto.client.ClientSaveDto;
import com.RotasVanApi.service.ClientService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "api/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping
    public ResponseEntity<ClientResponseDto> create(@RequestBody @Valid ClientSaveDto clientSaveDto){
        ClientResponseDto clientResponseDto = clientService.create(clientSaveDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(clientResponseDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable(value = "id") UUID id){
        clientService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Cliente deletado com sucesso!");
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientResponseDto> findById(@PathVariable(value = "id") UUID id){
        return ResponseEntity.status(HttpStatus.OK).body(clientService.findById(id));
    }

    @PutMapping("/update-token/{id}/{token}")
    public void updateTokenFirebase(@PathVariable(value = "id") UUID id,
                                    @PathVariable(value = "token") String token){

        clientService.updateFirebaseToken(id, token);
    }

}

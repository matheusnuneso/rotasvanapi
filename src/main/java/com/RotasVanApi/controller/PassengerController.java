package com.RotasVanApi.controller;

import com.RotasVanApi.dto.passenger.PassengerResponseDto;
import com.RotasVanApi.dto.passenger.PassengerSaveDto;
import com.RotasVanApi.dto.passenger.PassengerWithAddressDto;
import com.RotasVanApi.model.PassengerModel;
import com.RotasVanApi.service.PassengerService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "api/passenger")
public class PassengerController {

    @Autowired
    private PassengerService passengerService;

    @PostMapping
    public ResponseEntity<PassengerResponseDto> create(@RequestBody @Valid PassengerSaveDto passengerSaveDto){
        PassengerResponseDto passengerResponseDto = passengerService.create(passengerSaveDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(passengerResponseDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable(value = "id") UUID id){
        passengerService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Passenger deleted successfully");
    }

    @GetMapping("/{id}")
    public ResponseEntity<PassengerResponseDto> findById(@PathVariable(value = "id") UUID id){
        return ResponseEntity.status(HttpStatus.OK).body(passengerService.findById(id));
    }

    @GetMapping("client-id/{id}")
    public ResponseEntity<List<PassengerResponseDto>> findByClientId(@PathVariable(value = "id") UUID clientId){
        return ResponseEntity.status(HttpStatus.OK).body(passengerService.findByClientId(clientId));
    }

    @GetMapping("all-passengers/{id}")
    public ResponseEntity<List<PassengerWithAddressDto>> findAllPassengers(@PathVariable(value = "id") UUID userId){
        return ResponseEntity.status(HttpStatus.OK).body(passengerService.findAllPassengers(userId));
    }

    @GetMapping("near-driver/{driverId}/{dateSearch}/{dist}")
    public ResponseEntity<List<PassengerResponseDto>> findPassengerNearDriver(
            @PathVariable(value = "driverId") UUID driverid,
            @PathVariable("dateSearch") String dateSearch,
            @PathVariable("dist") String dist
    ){
        List<PassengerResponseDto> passengers = passengerService.findPassengersNearDriver(driverid, dateSearch, dist);
        return ResponseEntity.status(HttpStatus.OK).body(passengers);
    }
}

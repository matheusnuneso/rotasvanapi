package com.RotasVanApi.controller;

import com.RotasVanApi.dto.manage_attendance.ManageAttendanceDto;
import com.RotasVanApi.dto.manage_attendance.ManageAttendanceResponseDto;
import com.RotasVanApi.dto.passenger.PassengerResponseDto;
import com.RotasVanApi.dto.passenger.PassengerWithAddressDto;
import com.RotasVanApi.service.ManageAttendanceService;
import com.fasterxml.jackson.databind.JsonNode;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "api/manage-attendance")
public class ManageAttendanceController {

    @Autowired
    private ManageAttendanceService manageAttendanceService;

    @PostMapping
    public ResponseEntity<ManageAttendanceResponseDto> create(@RequestBody @Valid ManageAttendanceDto manageAttendanceDto){
        ManageAttendanceResponseDto manageAttendanceResponseDto = manageAttendanceService.create(manageAttendanceDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(manageAttendanceResponseDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable(value = "id") UUID id){
        manageAttendanceService.delete(id);
        return ResponseEntity.ok("Deleted successfully!");
    }

    @GetMapping("outward-route/{driverId}/{dateSearch}")
    public JsonNode getRouteOutwardTrip(@PathVariable(value = "driverId") UUID driverId, @PathVariable(value = "dateSearch") String dateSearch) throws Exception {
        return manageAttendanceService.getRouteOutwardTrip(driverId, dateSearch);
    }

    @GetMapping("return-route/{driverId}/{dateSearch}")
    public JsonNode getRouteReturnTrip(@PathVariable(value = "driverId") UUID driverId, @PathVariable(value = "dateSearch") String dateSearch) throws Exception {
        return manageAttendanceService.getRouteReturnTrip(driverId, dateSearch);
    }

    @GetMapping("outward/{driverId}/{dateSearch}")
    public List<PassengerWithAddressDto> getPassengersOutwardTrip(@PathVariable(value = "driverId") UUID driverId, @PathVariable(value = "dateSearch") String dateSearch) throws Exception {
        return manageAttendanceService.getPassengerOutwardTrip(driverId, dateSearch);
    }

    @GetMapping("return/{driverId}/{dateSearch}")
    public List<PassengerWithAddressDto> getPassengersReturnTrip(@PathVariable(value = "driverId") UUID driverId, @PathVariable(value = "dateSearch") String dateSearch) throws Exception {
        return manageAttendanceService.getPassengerReturnTrip(driverId, dateSearch);
    }

}

package com.RotasVanApi.controller;

import com.RotasVanApi.dto.address.AddressDto;
import com.RotasVanApi.model.AddressModel;
import com.RotasVanApi.service.AddressService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "api/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @PostMapping
    public ResponseEntity<AddressModel> create(@RequestBody @Valid AddressDto addressDto){
        AddressModel addressModel = addressService.create(addressDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(addressModel);
    }

}

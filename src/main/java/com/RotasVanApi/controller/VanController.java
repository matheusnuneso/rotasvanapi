package com.RotasVanApi.controller;

import com.RotasVanApi.dto.address.AddressDto;
import com.RotasVanApi.dto.van.VanSaveDto;
import com.RotasVanApi.model.PassengerModel;
import com.RotasVanApi.model.VanModel;
import com.RotasVanApi.service.DriverService;
import com.RotasVanApi.service.PassengerService;
import com.RotasVanApi.service.VanService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "api/van")
public class VanController {

    @Autowired
    private VanService vanService;
    @Autowired
    private PassengerService passengerService;
    @Autowired
    private DriverService driverService;

    @PostMapping
    public ResponseEntity<VanModel> create(@RequestBody @Valid VanSaveDto vanSaveDto){
        VanModel vanModel = vanService.create(vanSaveDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(vanModel);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VanModel> findById(@PathVariable(value = "id") UUID id){
        VanModel van = this.vanService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(van);
    }

    @PutMapping("/{driverId}/status/{status}")
    public ResponseEntity<String> changeStatusTrip(
            @PathVariable(value = "driverId") UUID driverId,
            @PathVariable(value = "status") Boolean status){

        UUID vanId = driverService.findById(driverId).getVanId();
        this.vanService.changeStatusTrip(vanId, status);
        return ResponseEntity.status(HttpStatus.OK).body("Status update successfully");
    }

    @PutMapping("/{driverId}")
    public ResponseEntity<String> updateCurrentLocation(
            @PathVariable(value = "driverId") UUID driverId,
            @RequestBody @Valid AddressDto location){

        System.out.println("ATUALIZOU A LOCALIZAÇÂO");
        System.out.println(location.getLatitude() + " ---- " + location.getLongitude());

        VanModel van = driverService.findByIdReturnModel(driverId).getVanModel();
        vanService.updateCurrentLocation(van, location);
        return ResponseEntity.status(HttpStatus.OK).body("Current location update successfully");
    }

    @GetMapping("passenger/{id}")
    public ResponseEntity<VanModel> findByPassengerId(@PathVariable(value = "id") UUID passengerId){
        PassengerModel passenger = passengerService.findByIdReturnModel(passengerId);
        return ResponseEntity.status(HttpStatus.OK).body(passenger.getVanModel());
    }
}

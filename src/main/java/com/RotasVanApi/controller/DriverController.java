package com.RotasVanApi.controller;

import com.RotasVanApi.dto.driver.DriverResponseDto;
import com.RotasVanApi.dto.driver.DriverSaveDto;
import com.RotasVanApi.service.DriverService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "api/driver")
public class DriverController {

    @Autowired
    private DriverService driverService;

    @PostMapping
    public ResponseEntity<DriverResponseDto> create(@RequestBody @Valid DriverSaveDto driverSaveDto){
        DriverResponseDto driverResponseDto = driverService.create(driverSaveDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(driverResponseDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable(value = "id") UUID id){
        driverService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Motorista deletado com sucesso!");
    }

    @GetMapping("/{id}")
    public ResponseEntity<DriverResponseDto> findById(@PathVariable(value = "id") UUID id){
        return ResponseEntity.status(HttpStatus.OK).body(driverService.findById(id));
    }

}

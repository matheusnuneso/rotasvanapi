package com.RotasVanApi.infra.requests;

import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.AddressModel;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class Requests {

    @Value("${api.mapbox.access.token}")
    private String accessToken;
    private String BASE_URL = "https://api.mapbox.com/optimized-trips/v1/mapbox/driving/";
    private String PARAMETERS = "?steps=true&geometries=geojson&access_token=";

    public JsonNode getOptimizedRoute(List<AddressModel> listCoordinates) throws Exception {
        String url = createStringURL(listCoordinates);

        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper objectMapper = new ObjectMapper();

        try {

            ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
            String responseBody = responseEntity.getBody();

            return objectMapper.readTree(responseBody);

        } catch (Exception e) {
            throw new CustomNotFoundException("Routes not available!");
        }
    }

    private String createStringURL(List<AddressModel> listCoordinates) {
        String coordinates = listCoordinates.stream()
                .map(address -> address.getLongitude() + "," + address.getLatitude())
                .collect(Collectors.joining(";"));

        return BASE_URL + coordinates + PARAMETERS + accessToken;
    }

}

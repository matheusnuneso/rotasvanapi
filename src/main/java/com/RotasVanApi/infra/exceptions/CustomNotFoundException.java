package com.RotasVanApi.infra.exceptions;

public class CustomNotFoundException extends RuntimeException{
    private String message;

    public CustomNotFoundException() {}

    public CustomNotFoundException(String msg) {
        super(msg);
        this.message = msg;
    }
}

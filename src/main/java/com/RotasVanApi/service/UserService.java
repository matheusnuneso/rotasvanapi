package com.RotasVanApi.service;

import com.RotasVanApi.dto.user.UserAuthDto;
import com.RotasVanApi.dto.user.UserSaveDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.UserModel;
import com.RotasVanApi.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public UserModel create(UserSaveDto userSaveDto){
        UserModel userModel = new UserModel();
        BeanUtils.copyProperties(userSaveDto, userModel);
        return userRepository.save(userModel);
    }

    @Transactional
    public void delete(UUID id){
        userRepository.deleteById(id);
    }

    public boolean existsByUserName(String userName){
        return userRepository.existsByUserName(userName);
    }

    public UserDetails findByUserName(String userName) throws UsernameNotFoundException {
        return userRepository.findByUserName(userName);
    }

    public void authenticationUser(UserAuthDto userToAuth, UserModel userDB){

        if (!userDB.getPassword().equals(userToAuth.getPassword())) {
            throw new CustomNotFoundException("User or password incorrect!");
        }

    }

}

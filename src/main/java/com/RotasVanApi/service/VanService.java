package com.RotasVanApi.service;

import com.RotasVanApi.dto.address.AddressDto;
import com.RotasVanApi.dto.client.ClientNotifDto;
import com.RotasVanApi.dto.van.VanSaveDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.infra.notifications.NotificationService;
import com.RotasVanApi.model.NotificationsSent;
import com.RotasVanApi.model.VanModel;
import com.RotasVanApi.repository.VanRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;

@Service
public class VanService {

    @Autowired
    private VanRepository vanRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private NotificationSentService notificationSentService;

    @Transactional
    public VanModel create(VanSaveDto vanSaveDto){
        VanModel vanModel = new VanModel(vanSaveDto);
        return vanRepository.save(vanModel);
    }

    public VanModel findById(UUID vanId){
        Optional<VanModel> vanModel = vanRepository.findById(vanId);

        if (vanModel.isEmpty()){
            throw new CustomNotFoundException("Van not found!");
        }

        return vanModel.get();
    }

    public void changeStatusTrip(UUID vanId, Boolean status){
        VanModel van = this.findById(vanId);
        van.setStartedTrip(status);

        vanRepository.save(van);
    }

    public void updateCurrentLocation(VanModel van, AddressDto location){
        String latitude = location.getLatitude();
        String longitude = location.getLongitude();

        if (!(isValidLatitude(latitude) || isValidLongitude(longitude))){
            throw new DateTimeException("Incorret coordinate format!");
        }

        van.setCurrentLatitude(latitude);
        van.setCurrentLongitude(longitude);
        vanRepository.save(van);

        sendNotifToClientsNearVan(van);
    }

    private void sendNotifToClientsNearVan(VanModel van) {
        LocalDate todayDate = LocalDate.now();
        List<ClientNotifDto> clientsTokens = returnTokensClientsNearVan(van, todayDate, 800);

        String title = "Van Chegando!";
        String body = "A van está a menos de 800 metros de você";

        for (ClientNotifDto clientModel: clientsTokens) {
            if ((clientModel.getToken() != null) && (!clientModel.getToken().isEmpty())){
                notificationService.sendNotification(clientModel.getToken(), title, body);
                notificationSentService.create(new NotificationsSent(clientModel.getId(), todayDate));
            }
        }
    }

    public boolean isValidLatitude(String latitudeStr) {
        String latitudePattern = "^([-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?))$";
        return latitudeStr.matches(latitudePattern);
    }

    public boolean isValidLongitude(String longitudeStr) {
        String longitudePattern = "^([-+]?([1]?[0-7]?\\d(\\.\\d+)?|180(\\.0+)?))$";
        return longitudeStr.matches(longitudePattern);
    }

    public List<ClientNotifDto> returnTokensClientsNearVan(VanModel van, LocalDate dateAbsence, Integer dist){
        Double driverLon = Double.valueOf(van.getCurrentLongitude());
        Double driverLat = Double.valueOf(van.getCurrentLatitude());

        return vanRepository.returnClientsNearVan(
                driverLon, driverLat, dist, van.getId(), dateAbsence);
    }
}

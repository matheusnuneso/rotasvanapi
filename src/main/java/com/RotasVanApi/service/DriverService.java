package com.RotasVanApi.service;

import com.RotasVanApi.dto.driver.DriverResponseDto;
import com.RotasVanApi.dto.driver.DriverSaveDto;
import com.RotasVanApi.dto.user.UserSaveDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.DriverModel;
import com.RotasVanApi.model.UserModel;
import com.RotasVanApi.model.VanModel;
import com.RotasVanApi.repository.DriverRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class DriverService {

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private UserService userService;
    @Autowired
    private VanService vanService;

    @Transactional
    public DriverResponseDto create(DriverSaveDto driverSaveDto){

        if (userService.existsByUserName(driverSaveDto.getUserName())){
            throw new EntityExistsException("UserName already exist!");
        }

        VanModel vanModel = vanService.findById(driverSaveDto.getVanId());

        UserModel userModel = userService.create(new UserSaveDto(driverSaveDto));

        DriverModel driverModel = new DriverModel(driverSaveDto, userModel, vanModel);
        DriverModel driverSaved = driverRepository.save(driverModel);

        return new DriverResponseDto(driverSaved);
    }

    @Transactional
    public void delete(UUID id){
        Optional<DriverModel> driverModel = driverRepository.findById(id);

        if (driverModel.isEmpty()){
            throw new CustomNotFoundException("Driver not found: " + id);
        }

        UUID userId = driverModel.get().getUser().getId();

        driverRepository.deleteById(id);
        userService.delete(userId);
    }

    public DriverResponseDto findById(UUID id){
        Optional<DriverModel> driverModel = driverRepository.findById(id);

        if (driverModel.isEmpty()){
            throw new CustomNotFoundException("Driver not found: " + id);
        }

        return new DriverResponseDto(driverModel.get());
    }

    public DriverModel findByUserId(UUID userId){
        Optional<DriverModel> driverModel = driverRepository.findByUserId(userId);

        if (driverModel.isEmpty()){
            throw new CustomNotFoundException("Driver not found!");
        }

        return driverModel.get();
    }

    public DriverResponseDto returnAuthDriver(UserModel userModel) {
        DriverModel driverModel = findByUserId(userModel.getId());
        return new DriverResponseDto(driverModel);
    }

    public DriverModel findByIdReturnModel(UUID id){
        Optional<DriverModel> driverModel = driverRepository.findById(id);

        if (driverModel.isEmpty()){
            throw new CustomNotFoundException("Driver not found!");
        }

        return driverModel.get();
    }
}

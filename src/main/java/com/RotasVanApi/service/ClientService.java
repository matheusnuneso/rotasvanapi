package com.RotasVanApi.service;

import com.RotasVanApi.dto.client.ClientResponseDto;
import com.RotasVanApi.dto.client.ClientSaveDto;
import com.RotasVanApi.dto.user.UserSaveDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.ClientModel;
import com.RotasVanApi.model.UserModel;
import com.RotasVanApi.repository.ClientRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private UserService userService;

    @Transactional
    public ClientResponseDto create(ClientSaveDto clientSaveDto){

        if (userService.existsByUserName(clientSaveDto.getUserName())){
            throw new EntityExistsException("UserName already exist!");
        }

        UserModel userModel = userService.create(new UserSaveDto(clientSaveDto));

        ClientModel clientModel = new ClientModel(clientSaveDto, userModel);
        ClientModel clientSaved = clientRepository.save(clientModel);

        return new ClientResponseDto(clientSaved);
    }

    @Transactional
    public void delete(UUID id){
        Optional<ClientModel> clientModel = clientRepository.findById(id);

        if (clientModel.isEmpty()){
            throw new CustomNotFoundException("Client not found: " + id);
        }

        UUID userId = clientModel.get().getUser().getId();

        clientRepository.deleteById(id);
        userService.delete(userId);
    }

    public ClientResponseDto findById(UUID id){
        Optional<ClientModel> clientModel = clientRepository.findById(id);

        if (clientModel.isEmpty()){
            throw new CustomNotFoundException("Client not found: " + id);
        }

        return new ClientResponseDto(clientModel.get());
    }

    public ClientModel findByIdReturnModel(UUID id){
        Optional<ClientModel> clientModel = clientRepository.findById(id);

        if (clientModel.isEmpty()){
            throw new CustomNotFoundException("Client not found: " + id);
        }

        return clientModel.get();
    }

    public ClientModel findByUserId(UUID userId){
        Optional<ClientModel> clientModel = clientRepository.findByUserId(userId);

        if (clientModel.isEmpty()){
            throw new CustomNotFoundException("Client not found!");
        }

        return clientModel.get();
    }

    public ClientResponseDto returnAuthClient(UserModel userModel) {
        ClientModel clientModel = findByUserId(userModel.getId());
        return new ClientResponseDto(clientModel);
    }

    public void updateFirebaseToken(UUID id, String token) {
        ClientModel clientModel = findByIdReturnModel(id);

        clientModel.setTokenFirebase(token);
        clientRepository.save(clientModel);
    }

    public String getTokenFirebase(UUID userId) {
        return clientRepository.findTokenFirebaseById(userId);
    }
}

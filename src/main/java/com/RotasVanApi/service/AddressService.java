package com.RotasVanApi.service;

import com.RotasVanApi.dto.address.AddressDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.AddressModel;
import com.RotasVanApi.repository.AddressRepository;
import jakarta.transaction.Transactional;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    @Transactional
    public AddressModel create(AddressDto addressDto){
        AddressModel addressModel = new AddressModel(addressDto);

        GeometryFactory geometryFactory = new GeometryFactory();
        Point point = geometryFactory.createPoint(new Coordinate(addressModel.getLongitude(), addressModel.getLatitude()));
        point.setSRID(4326);
        addressModel.setLocationPostgis(point);

        return addressRepository.save(addressModel);
    }

    @Transactional
    public void delete(UUID id){
        Optional<AddressModel> addressModel = addressRepository.findById(id);
        if (addressModel.isEmpty()){
            throw new CustomNotFoundException("Address not found: " + id);
        }
        addressRepository.deleteById(id);
    }

    public AddressModel findById(UUID id){
        Optional<AddressModel> addressModel = addressRepository.findById(id);

        if (addressModel.isEmpty()){
            throw new CustomNotFoundException("Address not found: " + id);
        }

        return addressModel.get();
    }
}

package com.RotasVanApi.service;

import com.RotasVanApi.dto.client.ClientResponseDto;
import com.RotasVanApi.dto.passenger.PassengerResponseDto;
import com.RotasVanApi.dto.passenger.PassengerSaveDto;
import com.RotasVanApi.dto.passenger.PassengerWithAddressDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.model.*;
import com.RotasVanApi.repository.PassengerRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PassengerService {

    @Autowired
    private PassengerRepository passengerRepository;
    @Autowired
    private ClientService clientService;
    @Autowired
    private VanService vanService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private DriverService driverService;

    @Transactional
    public PassengerResponseDto create(PassengerSaveDto passengerSaveDto){

        ClientModel client = clientService.findByIdReturnModel(passengerSaveDto.getClientId());
        VanModel van = vanService.findById(passengerSaveDto.getVanId());
        AddressModel originAd = addressService.findById(passengerSaveDto.getOriginAddressId());
        AddressModel destinyAd = addressService.findById(passengerSaveDto.getDestinyAddressId());

        PassengerModel passengerModel = passengerRepository.save(new PassengerModel(passengerSaveDto.getName(), client, van, originAd, destinyAd));

        return new PassengerResponseDto(passengerModel);
    }

    @Transactional
    public void delete(UUID passengerId){
        Optional<PassengerModel> passengerModel = passengerRepository.findById(passengerId);

        if (passengerModel.isEmpty()){
            throw new CustomNotFoundException("Passenger not found: " + passengerId);
        }

        passengerRepository.delete(passengerModel.get());
    }

    public PassengerResponseDto findById(UUID passengerId){
        Optional<PassengerModel> passengerModel = passengerRepository.findById(passengerId);

        if (passengerModel.isEmpty()){
            throw new CustomNotFoundException("Passenger not found: " + passengerId);
        }

        return new PassengerResponseDto(passengerModel.get());
    }

    public PassengerModel findByIdReturnModel(UUID passengerId){
        Optional<PassengerModel> passengerModel = passengerRepository.findById(passengerId);

        if (passengerModel.isEmpty()){
            throw new CustomNotFoundException("Passenger not found: " + passengerId);
        }

        return passengerModel.get();
    }

    public List<PassengerModel> findAll(){
        return passengerRepository.findAll();
    }

    public List<PassengerResponseDto> findByClientId(UUID clientId) {
        ClientModel client = clientService.findByIdReturnModel(clientId);

        return passengerRepository.findByClientModel(client).stream()
                .map(PassengerResponseDto::new)
                .collect(Collectors.toList());
    }

    public List<PassengerWithAddressDto> findAllPassengers(UUID driverId) {
        DriverModel driver = driverService.findByIdReturnModel(driverId);

        return passengerRepository.findByVanModel(driver.getVanModel()).stream()
                .map(passenger -> new PassengerWithAddressDto(passenger, "outward"))
                .collect(Collectors.toList());
    }

    public List<PassengerResponseDto> findPassengersNearDriver(UUID driverId, String dateSearch, String distStr){
        VanModel van = vanService.findById(driverService.findById(driverId).getVanId());

        if (!van.getStartedTrip()){
            throw new CustomNotFoundException("A viagem ainda não começou!");
        }

        Double driverLon = Double.valueOf(van.getCurrentLongitude());
        Double driverLat = Double.valueOf(van.getCurrentLatitude());
        LocalDate dateAbsence = LocalDate.parse(dateSearch);
        Integer dist = Integer.valueOf(distStr);

        List<PassengerModel> passengers = passengerRepository.passengersNearDriver(
                        driverLon, driverLat, dist, van.getId(), dateAbsence
                );

        return passengers.stream().map(PassengerResponseDto::new).collect(Collectors.toList());
    }
}

package com.RotasVanApi.service;

import com.RotasVanApi.dto.manage_attendance.ManageAttendanceDto;
import com.RotasVanApi.dto.manage_attendance.ManageAttendanceResponseDto;
import com.RotasVanApi.dto.passenger.PassengerResponseDto;
import com.RotasVanApi.dto.passenger.PassengerWithAddressDto;
import com.RotasVanApi.infra.exceptions.CustomNotFoundException;
import com.RotasVanApi.infra.requests.Requests;
import com.RotasVanApi.model.AddressModel;
import com.RotasVanApi.model.ManageAttendanceModel;
import com.RotasVanApi.model.PassengerModel;
import com.RotasVanApi.repository.ManageAttendanceRepository;
import com.fasterxml.jackson.databind.JsonNode;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ManageAttendanceService {

    @Autowired
    private Requests requests;

    @Autowired
    private ManageAttendanceRepository manageAttendanceRepository;

    @Autowired
    private PassengerService passengerService;

    @Autowired
    private DriverService driverService;

    @Transactional
    public ManageAttendanceResponseDto create(ManageAttendanceDto manageAttendanceDto){
        PassengerModel passengerModel = passengerService.findByIdReturnModel(manageAttendanceDto.getPassengerId());

        LocalDate dateAbsence = dateConverter(manageAttendanceDto.getDateAbsence());
        dateValidate(dateAbsence);

        ManageAttendanceModel manageAttendanceModel = new ManageAttendanceModel();
        manageAttendanceModel.setPassengerModel(passengerModel);
        manageAttendanceModel.setDateAbsence(dateAbsence);
        manageAttendanceModel.setOutwardTrip(manageAttendanceDto.getOutwardTrip());
        manageAttendanceModel.setReturnTrip(manageAttendanceDto.getReturnTrip());

        return new ManageAttendanceResponseDto(manageAttendanceRepository.save(manageAttendanceModel));
    }

    @Transactional
    public void delete(UUID manageAttendanceId){
        Optional<ManageAttendanceModel> manageAttendanceModel = manageAttendanceRepository.findById(manageAttendanceId);

        if (manageAttendanceModel.isEmpty()){
            throw new CustomNotFoundException("Not found: " + manageAttendanceId);
        }

        manageAttendanceRepository.delete(manageAttendanceModel.get());
    }

    public List<PassengerWithAddressDto> getPassengerOutwardTrip(UUID driverId, String dateSearch) {
        UUID vanId = driverService.findById(driverId).getVanId();
        LocalDate date = dateConverter(dateSearch);

        List<PassengerModel> passengerList = manageAttendanceRepository.findPassengersOutwardTrip(date, vanId);
        List<PassengerModel> allPassengers = passengerService.findAll();
        allPassengers.removeIf(passengerList::contains);

        return allPassengers.stream()
                .sorted(Comparator.comparing(PassengerModel::getName))
                .map(passenger -> new PassengerWithAddressDto(passenger, "outward"))
                .collect(Collectors.toList());
    }

    public List<PassengerWithAddressDto> getPassengerReturnTrip(UUID driverId, String dateSearch) {
        UUID vanId = driverService.findById(driverId).getVanId();
        LocalDate date = dateConverter(dateSearch);

        List<PassengerModel> passengerList = manageAttendanceRepository.findPassengersReturnTrip(date, vanId);
        List<PassengerModel> allPassengers = passengerService.findAll();
        allPassengers.removeIf(passengerList::contains);

        return allPassengers.stream()
                .sorted(Comparator.comparing(PassengerModel::getName))
                .map(passenger -> new PassengerWithAddressDto(passenger, "return"))
                .collect(Collectors.toList());
    }

    public JsonNode getRouteOutwardTrip(UUID driverId, String dateSearch) throws Exception {
        UUID vanId = driverService.findById(driverId).getVanId();
        LocalDate date = dateConverter(dateSearch);

        List<PassengerModel> passengerList = manageAttendanceRepository.findPassengersOutwardTrip(date, vanId);
        List<PassengerModel> allPassengers = passengerService.findAll();
        allPassengers.removeIf(passengerList::contains);

        List<AddressModel> originAddressList = getAllOriginAddress(allPassengers);

        return requests.getOptimizedRoute(originAddressList);
    }

    public JsonNode getRouteReturnTrip(UUID driverId, String dateSearch) throws Exception {
        UUID vanId = driverService.findById(driverId).getVanId();
        LocalDate date = dateConverter(dateSearch);

        List<PassengerModel> passengerList = manageAttendanceRepository.findPassengersReturnTrip(date, vanId);
        List<PassengerModel> allPassengers = passengerService.findAll();
        allPassengers.removeIf(passengerList::contains);

        List<AddressModel> destinyAddressList = getAllDestinyAddress(allPassengers);

        return requests.getOptimizedRoute(destinyAddressList);
    }

    private LocalDate dateConverter(String dateString){
        return LocalDate.parse(dateString);
    }

    private void dateValidate(LocalDate date){
        LocalDate todayDate = LocalDate.now();
        if (date.isBefore(todayDate)){
            throw new DateTimeException("Incorrect format date.");
        }
    }

    private List<AddressModel> getAllOriginAddress(List<PassengerModel> passengerList) {
        return passengerList.stream()
                .map(PassengerModel::getOriginAddressModel)
                .collect(Collectors.toList());
    }

    private List<AddressModel> getAllDestinyAddress(List<PassengerModel> passengerList) {
        return passengerList.stream()
                .map(PassengerModel::getDestinyAddressModel)
                .collect(Collectors.toList());
    }

}

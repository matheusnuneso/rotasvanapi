package com.RotasVanApi.service;

import com.RotasVanApi.model.NotificationsSent;
import com.RotasVanApi.repository.NotificationSentRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationSentService {

    @Autowired
    private NotificationSentRepository notificationSentRepository;

    @Transactional
    public NotificationsSent create(NotificationsSent notificationSent){
        return notificationSentRepository.save(notificationSent);
    }

}

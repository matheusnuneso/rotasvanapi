package com.RotasVanApi.model;

import com.RotasVanApi.dto.client.ClientSaveDto;
import com.RotasVanApi.enums.RoleEnum;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Table(name = "TB_CLIENT")
@Entity
@Data
@NoArgsConstructor
public class ClientModel {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @JoinColumn(name = "user_id")
    @OneToOne
    private UserModel user;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Boolean clientIsPassenger;

    @Column
    private String tokenFirebase;

    public ClientModel(ClientSaveDto clientSaveDto, UserModel userModel){
        this.user = userModel;
        this.name = clientSaveDto.getName();
        this.clientIsPassenger = clientSaveDto.getClientIsPassenger();
    }

}

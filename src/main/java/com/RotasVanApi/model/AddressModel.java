package com.RotasVanApi.model;

import com.RotasVanApi.dto.address.AddressDto;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.locationtech.jts.geom.Point;

import java.util.UUID;

@Table(name = "TB_ADDRESS")
@Entity
@Data
@NoArgsConstructor
public class AddressModel {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "desc_address", nullable = false)
    private String desc_address;

    @Column(nullable = false)
    private Double latitude;

    @Column(nullable = false)
    private Double longitude;

    @Column(columnDefinition = "geometry(Point, 4326)")
    private Point locationPostgis;

    public AddressModel(AddressDto addressDto){
        this.desc_address = addressDto.getDesc_address();
        this.latitude = Double.valueOf(addressDto.getLatitude());
        this.longitude = Double.valueOf(addressDto.getLongitude());
    }

}

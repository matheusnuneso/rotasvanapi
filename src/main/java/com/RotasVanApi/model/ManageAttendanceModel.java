package com.RotasVanApi.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

@Table(name = "TB_MANAGE_ATTENDANCE")
@Entity
@Data
@NoArgsConstructor
public class ManageAttendanceModel {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "passenger_id", nullable = false)
    private PassengerModel passengerModel;

    @Column(name = "date_absence", nullable = false, columnDefinition = "DATE")
    private LocalDate dateAbsence;

    @Column(name = "outward_trip")
    private Boolean outwardTrip;

    @Column(name = "return_trip")
    private Boolean returnTrip;

}

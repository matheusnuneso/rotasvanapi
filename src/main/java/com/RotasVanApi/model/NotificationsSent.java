package com.RotasVanApi.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@Table(name = "TB_NOTIFICATION_SENT")
@Entity
@Data
@AllArgsConstructor
public class NotificationsSent {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "client_id", nullable = false)
    private UUID clientId;

    @Column(name = "date_absence", nullable = false, columnDefinition = "DATE")
    private LocalDate dateAbsence;

    public NotificationsSent(UUID clientId, LocalDate dateAbsence){
        this.clientId = clientId;
        this.dateAbsence = dateAbsence;
    }

}

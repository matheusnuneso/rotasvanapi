package com.RotasVanApi.model;

import com.RotasVanApi.dto.driver.DriverSaveDto;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Table(name = "TB_DRIVER")
@Entity
@Data
@NoArgsConstructor
public class DriverModel {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @JoinColumn(name = "user_id")
    @OneToOne
    private UserModel user;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "van_id", nullable = false)
    private VanModel vanModel;

    public DriverModel(DriverSaveDto driverSaveDto, UserModel user, VanModel vanModel){
        this.user = user;
        this.name = driverSaveDto.getName();
        this.vanModel = vanModel;
    }
}

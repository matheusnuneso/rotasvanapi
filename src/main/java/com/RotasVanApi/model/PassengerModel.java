package com.RotasVanApi.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.UUID;

@Table(name = "TB_PASSENGER")
@Entity
@Data
@NoArgsConstructor
public class PassengerModel {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    private ClientModel clientModel;

    @ManyToOne
    @JoinColumn(name = "van_id", nullable = false)
    private VanModel vanModel;

    @ManyToOne
    @JoinColumn(name = "origin_address_id", nullable = false)
    private AddressModel originAddressModel;

    @ManyToOne
    @JoinColumn(name = "destiny_address_id", nullable = false)
    private AddressModel destinyAddressModel;

    public PassengerModel(String name, ClientModel client, VanModel van, AddressModel origin, AddressModel destiny){
        this.name = name;
        this.clientModel = client;
        this.vanModel = van;
        this.originAddressModel = origin;
        this.destinyAddressModel = destiny;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PassengerModel that = (PassengerModel) o;
        return id.equals(that.id);
    }
}

package com.RotasVanApi.model;

import com.RotasVanApi.dto.van.VanSaveDto;
import jakarta.persistence.*;
import lombok.Data;

import java.util.UUID;

@Table(name = "TB_VAN")
@Entity
@Data
public class VanModel {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(nullable = false)
    private String companyName;

    @Column(nullable = false)
    private Boolean startedTrip;

    @Column()
    private String currentLatitude;

    @Column()
    private String currentLongitude;

    public VanModel(VanSaveDto vanDto){
        this.companyName = vanDto.getCompanyName();
        this.startedTrip = false;
    }

    public VanModel(){}

}

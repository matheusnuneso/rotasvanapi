package com.RotasVanApi.repository;

import com.RotasVanApi.model.ClientModel;
import com.RotasVanApi.model.PassengerModel;
import com.RotasVanApi.model.VanModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.UUID;

@Repository
public interface PassengerRepository extends JpaRepository<PassengerModel, UUID> {
    ArrayList<PassengerModel> findByClientModel(ClientModel clientModel);

    ArrayList<PassengerModel> findByVanModel(VanModel vanModel);

    @Query(value = "SELECT pm.id, pm.name, pm.client_id, pm.destiny_address_id, pm.origin_address_id, pm.van_id  " +
            "FROM tb_passenger pm INNER JOIN tb_address am " +
            "ON pm.origin_address_id = am.id " +
            "WHERE ST_Intersects(ST_Transform(am.location_postgis, 31983), " +
            "    ST_Buffer(ST_Transform(ST_SetSRID(ST_Point( :driverLon ,  :driverLat ), 4326), 31983), :dist , 'quad_segs=8')) " +
            "AND pm.van_id = :vanId " +
            "AND NOT EXISTS ( " +
            "    SELECT 1 FROM tb_manage_attendance ma " +
            "    WHERE ma.passenger_id = pm.id " +
            "    AND ma.date_absence = :dateAbsence " +
            "    AND ma.outward_trip = true) ",
            nativeQuery = true)
    ArrayList<PassengerModel> passengersNearDriver(
            @Param("driverLon") Double driverLon,
            @Param("driverLat") Double driverLat,
            @Param("dist") Integer dist,
            @Param("vanId") UUID vanId,
            @Param("dateAbsence") LocalDate dateAbsence
    );
}

package com.RotasVanApi.repository;

import com.RotasVanApi.model.ManageAttendanceModel;
import com.RotasVanApi.model.PassengerModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface ManageAttendanceRepository extends JpaRepository<ManageAttendanceModel, UUID> {

    @Query("SELECT ma.passengerModel FROM ManageAttendanceModel ma " +
            "INNER JOIN ma.passengerModel ps " +
            "WHERE ma.dateAbsence = :dateAbsence " +
            "AND ma.outwardTrip = true " +
            "AND ps.vanModel.id = :vanId")
    List<PassengerModel> findPassengersOutwardTrip(@Param("dateAbsence") LocalDate dateAbsence, @Param("vanId") UUID vanId);

    @Query("SELECT ma.passengerModel FROM ManageAttendanceModel ma " +
            "INNER JOIN ma.passengerModel ps " +
            "WHERE ma.dateAbsence = :dateAbsence " +
            "AND ma.returnTrip = true " +
            "AND ps.vanModel.id = :vanId")
    List<PassengerModel> findPassengersReturnTrip(@Param("dateAbsence") LocalDate dateAbsence, @Param("vanId") UUID vanId);

}

package com.RotasVanApi.repository;

import com.RotasVanApi.model.DriverModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface DriverRepository extends JpaRepository<DriverModel, UUID> {

    Optional<DriverModel> findByUserId(UUID userId);
}

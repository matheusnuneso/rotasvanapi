package com.RotasVanApi.repository;

import com.RotasVanApi.dto.client.ClientNotifDto;
import com.RotasVanApi.model.VanModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.UUID;

@Repository
public interface VanRepository extends JpaRepository<VanModel, UUID> {
    @Query(value = "SELECT cm.id AS id, cm.token_firebase AS token " +
            "FROM tb_passenger pm INNER JOIN tb_address am " +
            "ON pm.origin_address_id = am.id " +
            "INNER JOIN tb_client cm ON pm.client_id = cm.id " +
            "WHERE ST_Intersects(ST_Transform(am.location_postgis, 31983), " +
            "    ST_Buffer(ST_Transform(ST_SetSRID(ST_Point( :driverLon ,  :driverLat ), 4326), 31983), :dist , 'quad_segs=8')) " +
            "AND pm.van_id = :vanId " +
            "AND NOT EXISTS ( " +
            "    SELECT 1 FROM tb_manage_attendance ma " +
            "    WHERE ma.passenger_id = pm.id " +
            "    AND ma.date_absence = :dateAbsence " +
            "    AND ma.outward_trip = true) " +
            "AND NOT EXISTS ( " +
            "    SELECT 1 FROM tb_notification_sent ns " +
            "    WHERE ns.client_id = cm.id " +
            "    AND ns.date_absence = :dateAbsence) ",
            nativeQuery = true)
    ArrayList<ClientNotifDto> returnClientsNearVan(
            @Param("driverLon") Double driverLon,
            @Param("driverLat") Double driverLat,
            @Param("dist") Integer dist,
            @Param("vanId") UUID vanId,
            @Param("dateAbsence") LocalDate dateAbsence
    );
}

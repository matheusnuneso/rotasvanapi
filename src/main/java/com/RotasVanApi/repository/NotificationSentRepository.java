package com.RotasVanApi.repository;

import com.RotasVanApi.model.NotificationsSent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NotificationSentRepository extends JpaRepository<NotificationsSent, UUID> {
}

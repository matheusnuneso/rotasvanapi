package com.RotasVanApi.repository;

import com.RotasVanApi.model.ClientModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ClientRepository extends JpaRepository<ClientModel, UUID> {

    boolean existsById(UUID id);

    Optional<ClientModel> findByUserId(UUID userId);

    @Query("SELECT c.tokenFirebase FROM ClientModel c WHERE c.id = :id")
    String findTokenFirebaseById(@Param("id") UUID id);

}

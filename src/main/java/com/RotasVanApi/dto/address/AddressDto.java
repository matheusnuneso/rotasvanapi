package com.RotasVanApi.dto.address;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class AddressDto {

    @NotNull
    private String desc_address;

    @NotBlank
    private String latitude;

    @NotBlank
    private String longitude;
}

package com.RotasVanApi.dto.passenger;

import com.RotasVanApi.model.AddressModel;
import com.RotasVanApi.model.PassengerModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassengerResponseDto {

    @NotNull
    private UUID id;

    @NotBlank
    private String name;

    @NotNull
    private UUID clientId;

    @NotNull
    private UUID vanId;

    @NotNull
    private UUID originAddressId;

    @NotNull
    private UUID destinyAddressId;

    public PassengerResponseDto(PassengerModel passengerModel) {
        this.id = passengerModel.getId();
        this.name = passengerModel.getName();
        this.clientId = passengerModel.getClientModel().getId();
        this.vanId = passengerModel.getVanModel().getId();
        this.originAddressId = passengerModel.getOriginAddressModel().getId();
        this.destinyAddressId = passengerModel.getOriginAddressModel().getId();
    }
}

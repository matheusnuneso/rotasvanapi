package com.RotasVanApi.dto.passenger;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassengerSaveDto {

    @NotBlank
    private String name;

    @NotNull
    private UUID clientId;

    @NotNull
    private UUID vanId;

    @NotNull
    private UUID originAddressId;

    @NotNull
    private UUID destinyAddressId;

}

package com.RotasVanApi.dto.passenger;

import com.RotasVanApi.model.PassengerModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassengerWithAddressDto {

    @NotNull
    private UUID id;

    @NotBlank
    private String name;

    @NotNull
    private UUID clientId;

    @NotNull
    private String descAddress;

    public PassengerWithAddressDto(PassengerModel passengerModel, String trip) {
        this.id = passengerModel.getId();
        this.name = passengerModel.getName();
        this.clientId = passengerModel.getClientModel().getId();

        if (trip.equals("outward")){
            this.descAddress = passengerModel.getOriginAddressModel().getDesc_address();
        } else {
            this.descAddress = passengerModel.getDestinyAddressModel().getDesc_address();
        }
    }
}

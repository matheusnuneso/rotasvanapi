package com.RotasVanApi.dto.manage_attendance;

import com.RotasVanApi.model.ManageAttendanceModel;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManageAttendanceResponseDto {

    @NotNull
    private UUID id;

    @NotNull
    private UUID passengerId;

    @NotNull
    private LocalDate dateAbsence;

    @NotNull
    private Boolean outwardTrip;

    @NotNull
    private Boolean returnTrip;

    public ManageAttendanceResponseDto(ManageAttendanceModel manageAttendanceModel) {
        this.id = manageAttendanceModel.getId();
        this.passengerId = manageAttendanceModel.getPassengerModel().getId();
        this.dateAbsence = manageAttendanceModel.getDateAbsence();
        this.outwardTrip = manageAttendanceModel.getOutwardTrip();
        this.returnTrip = manageAttendanceModel.getReturnTrip();
    }
}

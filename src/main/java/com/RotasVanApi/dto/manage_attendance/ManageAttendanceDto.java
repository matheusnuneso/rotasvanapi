package com.RotasVanApi.dto.manage_attendance;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManageAttendanceDto {

    @NotNull
    private UUID passengerId;

    @NotNull
    private String dateAbsence;

    @NotNull
    private Boolean outwardTrip;

    @NotNull
    private Boolean returnTrip;

}

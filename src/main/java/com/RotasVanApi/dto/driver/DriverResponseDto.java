package com.RotasVanApi.dto.driver;

import com.RotasVanApi.enums.RoleEnum;
import com.RotasVanApi.model.DriverModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Data
public class DriverResponseDto {

    @NotNull
    private UUID id;

    @NotBlank
    private String userName;

    @NotBlank
    private String name;

    private RoleEnum role = RoleEnum.DRIVER;

    @NotNull
    private UUID vanId;

    public DriverResponseDto(DriverModel driverModel){
        this.id = driverModel.getId();
        this.userName = driverModel.getUser().getUserName();
        this.name = driverModel.getName();
        this.vanId = driverModel.getVanModel().getId();
    }

}

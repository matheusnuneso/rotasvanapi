package com.RotasVanApi.dto.driver;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DriverSaveDto {

    @NotBlank
    private String userName;

    @NotBlank
    private String password;

    @NotBlank
    private String name;

    @NotNull
    private UUID vanId;

}

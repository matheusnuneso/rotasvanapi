package com.RotasVanApi.dto.van;

import com.RotasVanApi.model.DriverModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VanSaveDto {

    @NotBlank
    private String companyName;

}

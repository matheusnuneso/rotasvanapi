package com.RotasVanApi.dto.client;

import com.RotasVanApi.enums.RoleEnum;
import com.RotasVanApi.model.ClientModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Data
public class ClientResponseDto {

    @NotNull
    private UUID id;

    @NotBlank
    private String userName;

    @NotBlank
    private String name;

    @NotNull
    private Boolean clientIsPassenger;

    private RoleEnum role = RoleEnum.CLIENT;

    public ClientResponseDto(ClientModel clientModel){
        this.id = clientModel.getId();
        this.userName = clientModel.getUser().getUserName();
        this.name = clientModel.getName();
        this.clientIsPassenger = clientModel.getClientIsPassenger();
    }

}

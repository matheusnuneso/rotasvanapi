package com.RotasVanApi.dto.client;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientSaveDto {

    @NotBlank
    private String userName;

    @NotBlank
    private String password;

    @NotBlank
    private String name;

    @NotNull
    private Boolean clientIsPassenger;

}

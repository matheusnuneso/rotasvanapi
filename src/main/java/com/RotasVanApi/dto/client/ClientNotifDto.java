package com.RotasVanApi.dto.client;

import java.util.UUID;

public interface ClientNotifDto {
    UUID getId();
    String getToken();
}

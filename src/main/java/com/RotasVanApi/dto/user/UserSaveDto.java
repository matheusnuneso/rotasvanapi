package com.RotasVanApi.dto.user;

import com.RotasVanApi.dto.client.ClientSaveDto;
import com.RotasVanApi.dto.driver.DriverSaveDto;
import com.RotasVanApi.enums.RoleEnum;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.management.relation.Role;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSaveDto {
    @NotBlank
    private String userName;
    @NotBlank
    private String password;
    @NotNull
    private RoleEnum role;

    public UserSaveDto(ClientSaveDto clientSaveDto){
        this.userName = clientSaveDto.getUserName();
        this.password = clientSaveDto.getPassword();
        this.role = RoleEnum.CLIENT;
    }

    public UserSaveDto(DriverSaveDto driverSaveDto){
        this.userName = driverSaveDto.getUserName();
        this.password = driverSaveDto.getPassword();
        this.role = RoleEnum.DRIVER;
    }

}

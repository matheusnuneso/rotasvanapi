package com.RotasVanApi.dto.user;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UserAuthDto {

    @NotBlank
    private String userName;
    @NotBlank
    private String password;

}

package com.RotasVanApi.dto.user;

import com.RotasVanApi.enums.RoleEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
public class AuthResponseDto {

    private UUID id;
    private RoleEnum role;
    private String token;
    private List<UUID> passengers;
    private String tokenFirebase;
}
